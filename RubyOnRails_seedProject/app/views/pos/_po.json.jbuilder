json.extract! po, :id, :shop_id, :number, :created_at, :updated_at
json.url po_url(po, format: :json)
